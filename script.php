<?php

function check_email(string $email) {
    return preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $email);
}


function send_email(string $email, string $from, string $to, string $subj, string $body) {
    $header = 'From:' . $from . "\r\n";
    $header .= "Content-type: text/html\r\n";
    mail($to, $subj, $body, $header) ? true : false;
}

$host = 'localhost';
$db   = 'test';
$user = 'root';
$pass = '';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

$pdo = new PDO($dsn, $user, $pass, $opt);

$stmt = $pdo->prepare('SELECT email FROM users WHERE confirmed = ? AND validts < ?');


$stmt->execute([1, date('Y-m-d H:i:s')]);
$data = $stmt->fetchAll();

foreach ($data as $row) {

    if(check_email($row['email'])) {
        $stmt = $pdo->prepare('INSERT INTO  emails (email, checked, valid) VALUES (:email, :checked, :valid) ON DUPLICATE KEY UPDATE checked = :checked, valid = :valid');
        $valid = 1;
        $checked = 1;
        $stmt->bindParam(':email', $row['email'], PDO::PARAM_STR);
        $stmt->bindParam(':checked', $valid, PDO::PARAM_INT);
        $stmt->bindParam(':valid', $checked, PDO::PARAM_INT);
        $stmt->execute();
    }
}

$stmt = $pdo->prepare('SELECT email FROM emails WHERE checked = ? AND valid = ?');
$stmt->execute([1, 1]);
$data = $stmt->fetchAll();

foreach ($data as $row) {
    send_email($row['email']);
}

?>

